<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Customer extends Model
{
	use Sortable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'town', 'post_code', 'province', 'country', 'email', 'phone', 'vat_code', 'contact', 'notes'
    ];

	public $sortable = ['name', 'town', 'created_at'];
}
