<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use Illuminate\Support\Facades\Config;

class CustomerController extends Controller
{
    const PAGEITEMSNO = 5;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alpharows = Config::get('columnsortable.columns.alpha.rows', '.');
        array_push($alpharows, "town");
        Config::set('columnsortable.columns.alpha.rows', $alpharows);

        $customers = Customer::sortable()->paginate(self::PAGEITEMSNO);
        return view('customers.index',compact('customers'))
            ->with('i', (request()->input('page', 1) - 1) * self::PAGEITEMSNO);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->doValidation($request);

        Customer::create($request->all());

        return redirect()->route('customers.index')
                        ->with('success','Customer created successfully');
    }

    /**
     * Display the specified resource. Unused.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::find($id);

        return view('customers.show',compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);

        return view('customers.edit',compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->doValidation($request);

        Customer::find($id)->update($request->all());

        return redirect()->route('customers.index')
                        ->with('success','Customer updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Customer::find($id)->delete();

        return redirect()->route('customers.index')
                        ->with('success','Customer deleted successfully');
    }

    /**
     * Perform validation.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function doValidation(Request $request) {
        $this->validate($request, [
            'name' => 'required|max:191',
            'address' => 'required|max:191',
            'town' => 'required|max:191',
            'post_code' => 'required|max:191',
            'province' => 'required|max:191',
            'country' => 'required|max:191',
            'email' => 'nullable|email',
            'phone' => 'max:191',
            'vat_code' => 'numeric'
        ]);
    }
}
