<div class="form-group row">
    <div class="col-12 col-lg-6">
        <label for="name" class="mb-0">Name: *</label>
        {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-12 col-lg-6">
        <label for="address" class="mb-0">Address: *</label>
        {!! Form::text('address', null, array('placeholder' => 'Address','class' => 'form-control')) !!}
    </div>
    <div class="col-12 col-lg-4 col-md-8">
            <label for="town" class="mb-0">Town: *</label>
            {!! Form::text('town', null, array('placeholder' => 'Town','class' => 'form-control')) !!}
    </div>
    <div class="col-12 col-lg-2 col-md-4">
            <label for="post_code" class="mb-0">Post code: *</label>
            {!! Form::text('post_code', null, array('placeholder' => 'Post code','class' => 'form-control')) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-12 col-lg-6">
        <label for="province" class="mb-0">Province: *</label>
        {!! Form::text('province', null, array('placeholder' => 'Province','class' => 'form-control')) !!}
    </div>
    <div class="col-12 col-lg-6">
        <label for="country" class="mb-0">Country: *</label>
        {!! Form::text('country', null, array('placeholder' => 'Country','class' => 'form-control')) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-12 col-lg-6">
        <label for="email" class="mb-0">E-mail:</label>
        {!! Form::text('email', null, array('placeholder' => 'sales@company.com','class' => 'form-control')) !!}
    </div>
    <div class="col-12 col-lg-6">
        <label for="phone" class="mb-0">Phone:</label>
        {!! Form::text('phone', null, array('placeholder' => 'Phone','class' => 'form-control')) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-12 col-lg-4">
        <label for="vat_code" class="mb-0">VAT code: *</label>
        {!! Form::text('vat_code', null, array('placeholder' => 'VAT code','class' => 'form-control')) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-12 col-lg-6">
        <label for="contact" class="mb-0">Contact:</label>
        {!! Form::textarea('contact', null, array('placeholder' => 'Contact','class' => 'form-control','style'=>'height:150px')) !!}
    </div>
    <div class="col-12 col-lg-6">
        <label for="notes" class="mb-0">Notes:</label>
        {!! Form::textarea('notes', null, array('placeholder' => 'Notes','class' => 'form-control','style'=>'height:150px')) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-12 text-center">
        <button type="submit" class="btn btn-custom">Submit</button>
    </div>
    <div class="col-12">
        <span class="note">* Mandatory fields</span>
    </div>
</div>