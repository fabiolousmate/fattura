@extends('layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 mb-1">
            <div class="float-left">
                <h6 class="mt-3 page-title">Add new customer</h6>
            </div>
            <div class="float-right align-middle">
                <a class="btn btn-custom" href="{{ route('customers.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::open(array('route' => 'customers.store','method'=>'POST')) !!}
         @include('customers.form')
    {!! Form::close() !!}
@endsection