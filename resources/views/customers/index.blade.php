@extends('layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 mb-2">
            <div class="float-left">
                <h6 class="mt-3  page-title">Customers list</h6>
            </div>
            <div class="float-right">
                <a class="btn btn-custom" href="{{ route('customers.create') }}"> Create new customer</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">

    <table class="table table-sm table-striped table-responsive-sd">
        <tr class="row">
            <th class="col-sm-4">@sortablelink('name', 'Name')</th>
            <th class="col-sm-3">Address</th>
            <th class="col-sm-3">@sortablelink('town', 'Town')</th>
            <th class="col-sm-2">Action</th>
        </tr>

        @foreach ($customers as $customer)
        <tr class="row">
            <td class="col-sm-4">{{ $customer->name }}</td>
            <td class="col-sm-3">{{ $customer->address }}</td>
            <td class="col-sm-3">{{ $customer->town }}</td>
            <td class="col-sm-2">
                <a class="btn btn-sm btn-outline-custom" href="{{ route('customers.edit',$customer->id) }}">Edit</a>
                {!! Form::open(['method' => 'DELETE','route' => ['customers.destroy', $customer->id],'style'=>'display:inline', 'onsubmit' => "return confirmDeletion()"]) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-sm btn-outline-custom']) !!}
                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </table>

        </div>
    </div>
</div>


    {!! $customers->links('vendor.pagination.bootstrap-4') !!}
@endsection

@section('additionaljs')
function confirmDeletion() {
    var r = confirm("Do you confirm customer deletion?");
    return r;
}
@endsection
