@extends('layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 mb-1">
            <div class="float-left">
                <h6 class="mt-3  page-title">Edit customer</h6>
            </div>
            <div class="float-right">
                <a class="btn btn-custom" href="{{ route('customers.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::model($customer, ['method' => 'PATCH','route' => ['customers.update', $customer->id]]) !!}
        @include('customers.form')
    {!! Form::close() !!}
@endsection