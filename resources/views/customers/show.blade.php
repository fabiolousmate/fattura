@extends('layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 mb-1">
            <div class="float-left">
                <h6 class="mt-3">Show Customer</h6>
            </div>
            <div class="float-right">
                <a class="btn btn-custom" href="{{ route('customers.index') }}"> Back</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <strong>Name:</strong>
            {{ $customer->name }}
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <strong>Address:</strong>
            {{ $customer->address }}
        </div>

        <div class="col-12 mt-3">
            <span class="note">other fields...</span>
        </div>
    </div>
@endsection