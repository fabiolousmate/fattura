<!DOCTYPE html>

<html>
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Fattura Elettronica - test</title>
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
   		<link href="/css/style.css" rel="stylesheet">
	</head>

	<body>
		<div class="container">
		    <div class="row">
		        <div class="col-sm-12 mb-1 mt-1">
		        	<div class="logo">
						<a href="/"><img src="/images/logo.png" class="img-fluid" alt="Fattura Elettronica"></a>
					<div>
		        </div>
		    </div>

		    @yield('content')
		</div>

		<script>
			@yield('additionaljs')
		</script>
	</body>
</html>