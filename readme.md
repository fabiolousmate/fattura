## How to install

You need GIT installed on your computer, https://www.atlassian.com/git/tutorials/install-git  
You need Composer installed on your computer, https://getcomposer.org/download/  

STEPS:  

1) Create a new database on Mysql and set permissions for a user

2) Open .env file and setup accordingly the fields
DB_CONNECTION=mysql  
DB_HOST=127.0.0.1  
DB_PORT=3306  
DB_DATABASE=dbname (name of the database just created)  
DB_USERNAME=user (whatever)  
DB_PASSWORD=password (whatever)  

3) Launch these commands in the destinaton folder
git clone https://fabiolousmate@bitbucket.org/fabiolousmate/fattura.git  
cd fattura  
composer install  

4) Copy .env in the current folder, fattura

5) Create database tables, launching migrations
php artisan migrate  

6) Launch development server
php artisan serve  

7) Open your browser and load this URL
http://localhost:8000  

NOTE: .env file is not included in GIT repository.
